(function(NOIX, mediator){

    NOIX.RESOURCES = (function(){

    	var _resources = {};

    	var _set = function(module, options) {
    		_resources[module] = options;
    	};

    	var _get = function(module) {
    		return _resources[module];
    	};

    	var _addRoute = function(module, routeObject){
        NOIX.DEBUG.log("Adding...");
    		if(_checkParams(routeObject)) _resources[module].routes.push(routeObject);
    	};

    	var _addDefaultRoute = function(module, routeObject){
        if(_checkParams(routeObject)) _resources[module].defRoute = routeObject;
      };

      var _checkParams = function(routeObject) {

        for (var prop in routeObject) {
          if(prop != "path" && prop != "view" && prop != "controller") {
            NOIX.DEBUG.error("Invalid parameter when creating route: '" + prop + ":" + routeObject[prop] + "' should not be part of the route object.");
            return false;
          }

          if(typeof routeObject[prop] === "string") {} else {
            NOIX.DEBUG.error("Invalid parameter when creating route: '" + prop + ":" + routeObject[prop] + "' is not a String.");
            return false;
          }

          if(routeObject[prop] === "") {
            NOIX.DEBUG.error("Invalid parameter when creating route: '" + prop + ":" + routeObject[prop] + "' is empty.");
            return false;
          }
        }

        return true;
      };


    	mediator.subscribe("setResources", _set);
    	mediator.subscribe("getResources", _get);

    	return {
    		set : _set,
    		get : _get
    	};

    })();

})(window.NOIX = window.NOIX || {}, NOIX.MEDIATOR);