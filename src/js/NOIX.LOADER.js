(function(NOIX){

	NOIX.LOADER = {

		loadURL : function(url, callback, parent){
			parent = parent || document.querySelectorAll('[nx-view]')[0];
			callback = callback || function(){console.log("Default callback");};

			var xhr = new XMLHttpRequest();
			var t = new Date().getTime();
			xhr.onreadystatechange = function() {
				switch(xhr.readyState) {
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					case 4: // https://developer.mozilla.org/en-US/docs/Web/HTTP/Response_codes
						switch(xhr.status) {
							case 200:
							NOIX.DEBUG.log(url + " loaded in " + String(new Date().getTime() - t) + "ms", "LOADER");
							parent.innerHTML = xhr.responseText;
							callback();
							break;
							case 404:
							NOIX.DEBUG.error(url + " 404");
							window.location.href = "/#/404";
							break;
						}
						xhr = null;
						break;
				}
			};
			xhr.open("GET", url, true);
			xhr.send(null);
		},

		loadImage : function(url){

		},

		loadVideo : function(url){

		}

	};

})(window.NOIX = window.NOIX || {});