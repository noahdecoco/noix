(function(NOIX){

    NOIX.CONFIG = (function(){

    	var _config = {};

    	var _set = function(module, options) {
    		_config[module] = options;
    	};

    	var _get = function(module) {
    		return _config[module];
    	};

    	return {
    		set : _set,
    		get : _get
    	};

    })();

})(window.NOIX = window.NOIX || {});