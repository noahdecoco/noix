(function(NOIX){

    NOIX.MEDIATOR = (function(){

        var _events = {};
        
        var _subscribe = function(key, handler){
            NOIX.DEBUG.log("Subscribe: '" + key + "'", 4);
            if(_events.hasOwnProperty(key)) {
                _events[key].push(handler);
            } else {
                _events[key] = [handler];
            }
        };

        var _publish = function(key, data){
            if(_events[key]) {
                NOIX.DEBUG.log("Publish: '" + key + "'", 4);
                for(var i = 0; i < _events[key].length; i++){
                    _events[key][i].apply({}, data);
                }
            } else {
                NOIX.DEBUG.warn("Event doesn't exist: " + key);
            } 
        };

        var _remove = function(key, handler){
            if(_events.hasOwnProperty(key)) {
                if(handler){
                    if(_events[key].indexOf(handler) > -1){
                        _events[key].splice(_events[key].indexOf(handler) - 1, 1);
                    }
                } else {
                    delete _events[key];
                }
            }
        };

        var _removeAll = function(){
            _events = {};
        };

        return {
            subscribe : _subscribe,
            publish   : _publish,
            remove    : _remove,
            removeAll : _removeAll
        };

            
    })();
        

})(window.NOIX = window.NOIX || {});