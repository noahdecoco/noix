(function(NOIX, mediator){

  NOIX.APP = function(id, options){

    var _id = id;

    var _init = function() {

      NOIX.DEBUG.log("Init Module : " + _id);
      
      NOIX.CONFIG.set(_id, {
        environment: "",
        settings: {},
        title: "",
        description: "",
        googleKey : "",
        og : {}
      });
      
      NOIX.RESOURCES.set(_id, {
        routes: [],
        controllers: {},
        factories: {},
        services: {}
      });
      
    };


    var _run = function() {
      NOIX.DEBUG.log("Run Module : " + _id);
    };

    var _route = {
      when: function(routeObject){
        mediator.publish("addRoute", [_id, routeObject]);
        return this;
      },
      otherwise: function(params){
        mediator.publish("addDefaultRoute", [_id, params]);
      }
    };

    var _controller = function(key, ctrl){
      // NOIX.CONTROLLER.addController(key, ctrl);
      mediator.publish("addController", [_id, key, ctrl]);
    };

    var _factory = function(){
      NOIX.DEBUG.log("Creating a new factory");
    };

    var _service = function(){
      NOIX.DEBUG.log("Creating a new service");
    };

    var _resources = {
      set : function(){
        console.log("setting resource");
      },
      get : function(){
        console.log("getting resources");
        mediator.publish("getResources", [_id]);
      }
    };

    var _config = {
      set : function(){
        console.log("setting config");
      },
      get : function(){
        console.log("getting config");
      }
    };

    window.onload = function(){
      if(NOIX.CONFIG.hasOwnProperty(_id) || NOIX.RESOURCES.hasOwnProperty(_id)) {
        NOIX.DEBUG.error("Error creating module '" + _id + "'. Key already taken. Please use another name.");
        return;
      } else { 
        _init();
        _run();
        mediator.publish("startModule");
      }
    };

    return {
      route      : _route,
      controller : _controller,
      service    : _service,
      factory    : _factory,
      resources  : _resources,
      config     : _config
    };

  };

})(window.NOIX = window.NOIX || {}, NOIX.MEDIATOR);