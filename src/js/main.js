var app = new NOIX.APP("portfolio");

app.route
.when({
	path: "home",
	view: "views/home.html",
	controller: "homeCtrl"
})
.when({
	path: "about",
	view: "views/about.html",
	controller: "aboutCtrl"
})
.when({
	path: "projects/(.*)/(.*)/",
	view: "views/projects-specific.html",
	controller: "projectsCtrl"
})
.when({
	path: "projects/(.*)",
	view: "views/projects-genre.html",
	controller: "projectsCtrl"
})
.when({
	path: "projects",
	view: "views/projects.html",
	controller: "projectsCtrl"
})
.when({
	path: "404",
	view: "views/404.html"
})
.otherwise({
	path: "home"
});


app.controller("homeCtrl", function(){
	console.log("I AM THE HOME CONTROLLER!", arguments);
});

app.controller("aboutCtrl", function(){
	console.log("I AM THE ABOUT CONTROLLER!", arguments);
});

app.controller("projectsCtrl", function(genre, work){
	console.log("I AM THE PROJECTS CONTROLLER!", genre, work);

	if(document.getElementById("genre")) document.getElementById("genre").innerHTML = genre;
	if(document.getElementById("work")) document.getElementById("work").innerHTML = work;
});
