(function(NOIX, mediator){

  NOIX.CONTROLLER = (function(){

    var _resources = {
      controllers : []
    };

    var _addController = function(module, key, ctrl){
      _resources.controllers.push({
        "key"  : key,
        "ctrl" : ctrl
      });
    };

    var _runController = function(key, params){
      NOIX.DEBUG.log("Running controller: " + key + "[" + params + "]");
      for(var i = 0; i < _resources.controllers.length; i++){
        if(_resources.controllers[i].key === key) {
          _resources.controllers[i].ctrl.apply({}, params);
        }
      }
    };

    mediator.subscribe("addController", _addController);
    mediator.subscribe("runController", _runController);

  })();

})(window.NOIX = window.NOIX || {}, NOIX.MEDIATOR);