(function(NOIX, mediator){

  NOIX.ROUTER = (function(){
    
    var _resources = 
    {
      routes   : [],
      defRoute : {}
    };
    
    var _checkParams = function(routeObject) {
      
      for (var prop in routeObject) {
        if(prop != "path" && prop != "view" && prop != "controller") {
          NOIX.DEBUG.error("Invalid parameter when creating route: '" + prop + ":" + routeObject[prop] + "' should not be part of the route object.");
          return false;
        }
        
        if(typeof routeObject[prop] === "string") {} else {
          NOIX.DEBUG.error("Invalid parameter when creating route: '" + prop + ":" + routeObject[prop] + "' is not a String.");
          return false;
        }
        
        if(routeObject[prop] === "") {
          NOIX.DEBUG.error("Invalid parameter when creating route: '" + prop + ":" + routeObject[prop] + "' is empty.");
          return false;
        }
      }
      
      return true;
    };
    
    var _addRoute = function(module, routeObject){
      if(_checkParams(routeObject)) _resources.routes.push(routeObject);
    };
    
    var _addDefaultRoute = function(module, routeObject){
      if(_checkParams(routeObject)) _resources.defRoute = routeObject;
    };
    
    var _getRoute = function(){
      var route = window.location.href.split("#")[1];
      if(route === undefined || route.length === 0 || route === "/") {
        window.location.href = "/#/" + _resources.defRoute.path;
        return _resources.defRoute.path;
      }
      return String(route).replace(/^\//g, '').replace(/\/$/g, '');
    };
    
    var _checkRoute = function(route) {
      var _this = this;
      var view, ctrl, params;
      for (var i = 0; i < _resources.routes.length; i++) {
        var match = route.match(String(_resources.routes[i].path).replace(/^\//g, '').replace(/\/$/g, ''));
        if(match){
          match.shift();
          view = _resources.routes[i].view;
          ctrl = _resources.routes[i].controller;
          params = match;
          break;
        }
      }
      NOIX.LOADER.loadURL(_resources.routes[i].view, function(){
        // NOIX.CONTROLLER.runController(_resources.routes[i].controller, params);
        mediator.publish("runController", [_resources.routes[i].controller, params]);
      });
    };
    
    var _listen = function() {

      console.log("listening..");
      var _this = this;
      
      var current = _getRoute();
      _checkRoute(current);
      
      var routeCheck = function(){
        if(current != _getRoute()) {
          current = _getRoute();
          _checkRoute(current);
        }
      };
      
      _this.interval = setInterval(routeCheck, 100);
    };

    mediator.subscribe("addRoute", _addRoute);
    mediator.subscribe("addDefaultRoute", _addDefaultRoute);
    mediator.subscribe("startModule", _listen);

  })();

})(window.NOIX = window.NOIX || {}, NOIX.MEDIATOR);
