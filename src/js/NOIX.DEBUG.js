(function(NOIX){

    NOIX.DEBUG = (function(){

        var _config = {
            enabled : true,
            level  : 4
        };

        var _styles = {
            level_1 : "color: #b9e8bc;",
            level_2 : "color: #8ad88e;",
            level_3 : "color: #5bc762;",
            level_4 : "color: #3ba742;",

            level_warn    : "color: #F7A541;",
            level_success : "color: #A1DBB2;",
            level_error   : "color: #F45D4C;"
        };

        var _log = function(message, level) {

            if(!_config.enabled) return;
            if(level > _config.level) return;

            if(level !== undefined) {
                /*message = " " + message;
                for (var i = 0; i < level; i++) {
                    message = "-" + message;
                }*/
                console.log("%c"+message, _styles["level_"+level]);
            } else {
                console.log(message);
            }
        };

        var _warn = function(message) {
            if(_config.enabled) console.log("%c! " + message, _styles.level_warn);
        };

        var _error = function(message) {
            if(_config.enabled) console.log("%c!!! " + message + " !!!", _styles.level_error);
        };

        var _success = function(message) {
            if(_config.enabled) console.log("%c!!! " + message + " !!!", _styles.level_success);
        };

        var _timeFunction = function(func) {
            if(_config.enabled) {
                var d1 = new Date();
                func();
                var d2 = new Date();
                var t = d2 - d1;
                console.log("Function " + func.name + " took " + t + "ms to complete");
            }
        };

        return {
            log          : _log,
            warn         : _warn,
            error        : _error,
            success      : _success,
            timeFunction : _timeFunction
        };

    })();

})(window.NOIX = window.NOIX || {});


function testLogs(){
    NOIX.DEBUG.log("A regular log");
    NOIX.DEBUG.log("Debug log, level 1", 1);
    NOIX.DEBUG.log("Debug log, level 2", 2);
    NOIX.DEBUG.log("Debug log, level 3", 3);

    NOIX.DEBUG.warn("This is a warning");
    NOIX.DEBUG.error("This is an error");
    NOIX.DEBUG.success("This is an success");
}