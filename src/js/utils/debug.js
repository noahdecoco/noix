var debug = (function(){

	var _getTimeStamp = function() {
		var d = new Date();
		var h = String("0" + d.getHours()).slice(-2);
		var m = String("0" + d.getMinutes()).slice(-2);
		var s = String("0" + d.getSeconds()).slice(-2);
		var ms = String("0" + d.getMilliseconds()).slice(-2);
		return h + ":" + m + ":" + s + ":" + ms + " | ";
	};

	// String functions
	var _log = function() {
		console.log(_getTimeStamp() , arguments);
	};

	var _timeFunction = function(f) {
		var d1 = new Date().getTime();
		f();
		var d2 = new Date().getTime();
		_log(d2 - d1 + "ms");
	};

	// Object functions

	return {
		log : _log
	};

})();
