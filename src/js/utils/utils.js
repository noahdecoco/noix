var utils = (function(){

	// String functions
	var _clearSlashes = function(_string) {
		return _string.toString().replace(/\/$/, '').replace(/^\//, '');
	};

	// Object functions

	return {
		clearSlashes : _clearSlashes
	};

})();