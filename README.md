# NOIX #

A barebones framework built in vanilla Javascript to kickstart creating new Single Page Applications.

## Features ##
* Project folder structure
* Gruntfile to watch and compile
* Less files for styling
* A custom built debugger
* A custom built URL router
* A custom built asset loader
* API for creating routes and controllers


## ToDo ##
* Easily add any third party library with Bower
* Form validation and submission module
* API for setting configurations
* API for service creation
* API for factor creation


## Initialisation ##
```
#!javascript
var app = new NOIX.APP("myApp");
```
## Setting Routes ##
```
#!javascript
app.route
.when({
	path: "home",
	view: "views/home.html",
	controller: "homeCtrl"
})
.when({
	path: "about",
	view: "views/about.html",
	controller: "aboutCtrl"
})
```
## Setting Controllers ##
```
#!javascript
app.controller("homeCtrl", function(){
	console.log("I AM THE HOME CONTROLLER!");
});

app.controller("aboutCtrl", function(){
	console.log("I AM THE ABOUT CONTROLLER!");
});
```